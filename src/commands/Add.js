const Clapp = require('clapp');
const Firebase = require("../firebase/firebase")
const _ = require('lodash')

//This function accepts the user inputted race and returns a sanitized version
const checkRace = race =>{
    const i = race[0].toLowerCase();
    const races = {
        h: "Human",
        d: "Dwarf",
        n: "Night Elf",
        e: "Night Elf",
        g: "Gnome"
    }
    return(races[i] || 'Unknown');
}

//This function accepts the user inputted class and returns a sanitized version
const checkClass = characterClass =>{
    let i = characterClass[0].toLowerCase();
    if (i == 'p'){
        i = characterClass.slice(0,2).toLowerCase()
    } else if( i == 'w'){
        i = characterClass.slice(0,4).toLowerCase()
    }

    const classes = {
        d: "Druid",
        h: "Hunter",
        m: "Mage",
        pa: "Paladin",
        pr: "Priest",
        r: "Rogue",
        warl: "Warlock",
        warr: "Warrior",
    }

    return (classes[i] || 'Unknown') ;
}

const add = new Clapp.Command({
    name: "add",
    desc: "Adds a player to the kos list.",
    fn:  (argv, context) => {
        let time = new Date().getTime();    //gets a timestamp
        if (argv.args.name.length < 3) {    //verify that they didnt accidentally press enter after invoking   
            context.msg.reply('Please enter a valid character name.')
        } else if ((_.intersection(context.characterNames, [argv.args.name])).length > 0) {  
            //If the character to be added is already in the database, this section will ammend the object
            //It increases the number of times this character has been added to the list and 
            //updates the lastSeen variable to a current timestamp, then it saves the new data to the database
            let oldCharacterInfo = []
            let newData = []
            context.data.forEach(character => {
                if (character.name === argv.args.name) {
                    oldCharacterInfo.push(character)
                } else {
                    newData.push(character)
                }
            })

            //Need to add logic to handle what happens when the user adds race and class info for a
            //character who is already on the list but has 'Unknown' for race or class. 

            if (oldCharacterInfo[0].race == 'Unknown' && argv.flags.race != 'Unknown'){
                oldCharacterInfo[0].race = checkRace(argv.flags.race);
            }

            if (oldCharacterInfo[0].class == 'Unknown' && argv.flags.class != 'Unknown'){
                oldCharacterInfo[0].class = checkClass(argv.flags.class);
            }


            oldCharacterInfo[0].lastSeen = time;
            oldCharacterInfo[0].numberOfTimesSeen += 1;
            newData.push(oldCharacterInfo[0]);
            Firebase.database().ref("/characters").set(newData);
            context.msg.reply(argv.args.name + " is already on the list! Updating information")
        }
        else {
            //If the character isnt in the databse, this section adds all of the necessary info
            //to form a proper character object and then adds the data to the database
            let newData = context.data;

            let characterClass = argv.flags.class;
            let race = argv.flags.race;
            
            if (race != 'Unknown'){ //Sanitizes race input
                race = checkRace(race);
            }

            if (characterClass != 'Unknown') {  //Sanitizes class input
                characterClass = checkClass(characterClass)
            }

            newData.push({  //pushes the new character data to the temporary object
                name: argv.args.name,
                class: characterClass,
                race,
                firstSeen: time,
                lastSeen: time,
                numberOfTimesSeen: 1
            })
            Firebase.database().ref("/characters").set(newData);    //push the data to firebase
            context.msg.reply(argv.args.name + " has been added to the KOS list.")  //notify the user
        }

    },
    //This section defines the arguments and flags that the Add command accepts
    args: [
        new Clapp.Argument({
            name: "name",
            desc: "The characters name",
            type: "string",
            required: true,
            default: null
        })
    ],
    flags: [
        new Clapp.Flag({
            name: "class",
            desc: "The characters class",
            type: "string",
            alias: "c",
            default: 'Unknown'
        }),
        new Clapp.Flag({
            name: "race",
            desc: "The characters race",
            type: "string",
            alias: "r",
            default: 'Unknown'
        }),
    ],
})

module.exports = add;