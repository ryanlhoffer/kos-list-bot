const Clapp = require('clapp');

//This block defines the number flag for the list command.  This sets how many records to show when list is called. Default is 10
const number = new Clapp.Flag({
    name: "number",
    desc: "The number of characters that will be shown.",
    type: "number",
    alias: 'n',
    default: 10
})

const verbose = new Clapp.Flag({
    name: "verbose",
    desc: "Changes the display to verbose mode when called",
    type: "boolean",
    alias: 'v',
    default: false
})

const list = new Clapp.Command({
    name: "list",
    desc: "  Shows the KOS list.",
    fn:  (argv, context) => {
        const { characterNames, data, msg } = context;
        let {number, verbose} = argv.flags;
        let output = "\n";
        if (verbose) {
            if (data.length < number) { number = data.length; }
            //If there are more records in the database than the user wants to see, this block will display
            //the amount of numbers the user indicated
            for (let i = 0; i <= number - 1; i++) {
                if (data[i]) {
                    let {name, race } = data[i];
                    output += name + " | " + race + " | " + data[i].class + "\n"
                }

            }
        } else {
            output += characterNames.join(" | ")
        }
        msg.reply(output)
    },
    args: [],
    flags: [number, verbose]
})

module.exports = list;