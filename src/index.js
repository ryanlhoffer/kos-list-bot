const Discord = require('discord.js');
const client = new Discord.Client();

const Firebase = require('./firebase/firebase')

const _ = require('lodash');

const Clapp = require('clapp');
const Add = require('./commands/Add');
const List = require('./commands/List');

data = [];  //All data in the database
characterNames = [];    //An array of all character names in the database
lowercaseCharacterNames = [];

//Whenever the daatabase updates, update the variables the rest of the project will use
Firebase.database().ref('/characters').on('value', (snapshot) => {
    data = snapshot.val()
    data.forEach(character => {
        characterNames.push(character.name)
    })
    lowercaseCharacterNames = [];
    lowercaseCharacterNames = characterNames.join('|').toLowerCase().split('|');
});

const app = new Clapp.App({
    name: "KoS List",
    desc: "Add and view people on the KoS List",
    prefix: "!kos",
    version: "1.0",
    caseSensitive: false,
    commands: [Add, List],  //These commands are where the meat and potatoes of the logic reside. You can find them in the src/commands folder
    onReply:  (msg, context) => {
        // Clapp will use this function to
        // communicate with the end user


        if (msg.slice(0, 5) == 'Error') {
            //This block catches the output if Clapp errored and then checks to see if the user wanted to do a lookup.
            //If so, this then looks up the charactername and lets the user know if it is on the list or not
            const messageArray = context.msg.content.split(" ");
            if (messageArray[0] == app.prefix) {
                const arr = [];
                arr.push(messageArray[1].toLowerCase())
                if (_.intersection(arr, lowercaseCharacterNames).length > 0) {
                    msg = messageArray[1].toUpperCase() + " IS ON THE LIST!"
                } else {
                    msg = messageArray[1] + " isn't on the list."
                }
            }
        }
        //Forward the message to the discord channel
        context.msg.reply(msg)
    }
});

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);    //Prints a message when the bot connects
});

client.on('message', msg => {
    if (app.isCliSentence(msg.content)) {   //checks to see if the current message is a valid CLI sentence
        //It then parses the sentence and carries out the appropriate action.  Sends whatever is in the second
        //parameter as a payload that the Clapp docs refer to as "context"
        app.parseInput(msg.content, { msg, data, characterNames, lowercaseCharacterNames })
    }
});

client.login('NTY2Nzg4OTgwNjUwMjc4OTMz.XZJNyw.ITOwfX4uk67Cxi__nF-5g0CmLZI');